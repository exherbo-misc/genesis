/* vim: set sw=2 sts=2 et foldmethod=syntax : */
/*
 * Copyright (c) 2016 Saleem Abdulrasool <compnerd@compnerd.org>
 *
 * This file is part of the Genesis initsystem. Genesis is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Genesis is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef genesis_util_scoped_fd_hh
#define genesis_util_scoped_fd_hh

namespace genesis {
namespace util {
template <typename Traits>
class basic_scoped_fd {
public:
  using traits_type = Traits;

private:
  typename traits_type::value_type fd_;

  void free() {
    if (fd_ == traits_type::invalid())
      return;
    traits_type::free(fd_);
    fd_ = traits_type::invalid();
  }

public:
  basic_scoped_fd(const basic_scoped_fd &) = delete;
  basic_scoped_fd & operator=(const basic_scoped_fd &) = delete;

  template <typename RHSTraits>
  bool operator==(const basic_scoped_fd<RHSTraits> &) const = delete;

  template <typename RHSTraits>
  bool operator!=(const basic_scoped_fd<RHSTraits> &) const = delete;

  basic_scoped_fd() : fd_(traits_type::invalid()) {}
  explicit basic_scoped_fd(typename traits_type::value_type fd) : fd_(fd) {}
  basic_scoped_fd(basic_scoped_fd<Traits> &&rhs) : fd_(rhs.release()) {}

  ~basic_scoped_fd() { free(); }

  basic_scoped_fd<Traits> &operator=(basic_scoped_fd<Traits> &&rhs) {
    reset(rhs.release());
    return *this;
  }

  void reset(typename traits_type::value_type fd = traits_type::invalid()) {
    if (fd_ != traits_type::invalid() && fd_ == fd)
      abort();
    free();
    fd_ = fd;
  }

  [[gnu::warn_unused_result]] typename traits_type::value_type release() {
    typename traits_type::value_type value = fd_;
    fd_ = traits_type::invalid();
    return value;
  }

  operator typename traits_type::value_type() const { return fd_; }

  bool invalid() const { return fd_ == traits_type::invalid(); }
};
}
}

#if defined(__linux__) || defined(__FreeBSD__) || defined(__NetBSD__) ||       \
    defined(__DragonFlyBSD__) || defined(__OpenBSD__) || defined(__MACH__)
#include <stdio.h>
#include <unistd.h>

namespace genesis {
namespace util {
struct UnixFDTraits {
  using value_type = int;
  static const value_type invalid() { return -1; }
  static void free(value_type fd) {
    int result = ::close(fd);
    if (result == 0 || errno == EINTR)
      return;
    ::perror("close");
  }
};

using scoped_fd = basic_scoped_fd<UnixFDTraits>;
}
}
#else
#error unknown file descriptor type
#endif

#endif

