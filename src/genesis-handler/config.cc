/* vim: set sw=2 sts=2 et foldmethod=syntax : */

/*
 * Copyright (c) 2010 Bryan Østergaard
 *
 * This file is part of the Genesis initsystem. Genesis is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Genesis is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <fstream>
#include <iostream>
#include <regex>

#include "genesis-handler/config.hh"

void Configuration::Construct(const std::string &file,
                              const std::string &section_name) {
  static const std::regex kv_regex =
      std::regex("[ \t]*([a-zA-Z_-]+)[ \t]*=[ \t]*([a-zA-Z0-9/._]+)[ \t]*");

  std::ifstream config(file.c_str());

  if (config) {
    while (!config.eof()) {
      std::string line;
      std::getline(config, line);

      // Remove comments
      line = std::regex_replace(line, std::regex("#.*"), "");

      if (section == section_name) {
        std::smatch sections;
        if (std::regex_match(line, sections,
                             std::regex("[ \t]*\\[([a-zA-Z]+)\\][ \t]*")))
          section = sections[0].str();

        std::smatch kv;
        if (std::regex_match(line, kv, kv_regex))
          options[kv[0].str()] = kv[1].str();
      } else if (line[0] == '[') {
        section = line.substr(1, line.size() - 2);
      }
    }
  }

  section = section_name;
}

