/* vim: set sw=2 sts=2 et foldmethod=syntax : */
/*
 * Copyright (c) 2010 Bryan Østergaard
 *
 * This file is part of the Genesis initsystem. Genesis is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Genesis is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "genesis-handler/event-listener.hh"
#include "util/log.hh"

#include <utility>

#define EVENT_LISTENER ("Event-Listener")

using namespace genesis;
using namespace genesis::events;
using namespace genesis::logging;

void EventListener::listen() {
  int maxfd = 0;
  fd_set readfds;
  FD_ZERO(&readfds);

  for (const auto &manager : _managers) {
    if (manager.first >= 0) {
      FD_SET(manager.first, &readfds);
      maxfd = std::max(manager.first, maxfd);
    }
  }

  for (const auto &source : _sources) {
    if (source.first >= 0) {
      FD_SET(source.first, &readfds);
      maxfd = std::max(source.first, maxfd);
    }
  }

  ::select(maxfd + 1, &readfds, NULL, NULL, NULL);

  for (const auto &manager : _managers) {
    if (FD_ISSET(manager.first, &readfds)) {
      std::unique_ptr<Action> action(manager.second->GetEvent());

      if (action.get()) {
        Log::get_instance().log(INFO, EVENT_LISTENER,
                                "received action: " + action->Identity());
        action->Execute();
        Log::get_instance().log(DEBUG, EVENT_LISTENER,
                                "action result: " + action->GetResult());

        _events.push_back(action->Identity());
      }
    }
  }

  for (const auto &source : _sources) {
    if (FD_ISSET(source.first, &readfds)) {
      std::unique_ptr<Action> action(source.second->process_event());

      if (action.get()) {
        Log::get_instance().log(INFO, EVENT_LISTENER,
                                "received action: " + action->Identity());
        action->Execute();
        Log::get_instance().log(DEBUG, EVENT_LISTENER,
                                "action result: " + action->GetResult());

        _events.push_back(action->Identity());
      }
    }
  }
}

void EventListener::process_eventqueue() {
  const std::list<std::string> events(_events);
  _events.clear();

  for (const auto &event : events) {
    for (const auto &manager : _managers) {
      std::unique_ptr<Action> action(manager.second->new_event(event));

      if (action.get()) {
        Log::get_instance().log(INFO, EVENT_LISTENER,
                                "received action: " + action->Identity());
        action->Execute();
        Log::get_instance().log(DEBUG, EVENT_LISTENER,
                                "action result: " + action->GetResult());

        _events.push_back(action->Identity());
      }
    }
  }
}

